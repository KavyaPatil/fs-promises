const fs = require("fs");
const { resolve } = require("path");



function deleteFiles(dirName, files) {
  console.log(files.length);
  for (let index = 0; index < files.length; index++) {
    fs.unlink(`${dirName}/${files[index]}.json`, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("deleted file successfully");
      }
    });
  }
}



let count = 0;

function writeFiles(dirName, number) {
  const content = JSON.stringify("Hello World!");

  const arrayOfFileNames = [];

  return new Promise((resolve, reject) => {
    for (let index = 1; index <= number; index++) {
      const fileName = `file_${index}`;

      arrayOfFileNames.push(fileName);
      fs.writeFile(`${dirName}/${fileName}.json`, content, (err) => {
        if (err) {
          reject(err);
        } else {
          console.log("creating file success");
          count++;
          if (count === number) {
            console.log(arrayOfFileNames, "writefile");
            resolve(arrayOfFileNames);
          }
        }
      });
    }
  });
}



function createDirectories(dirName) {
  return new Promise((resolve, reject) => {
    fs.mkdir(dirName, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve("creating directory success");
      }
    });
  });
}

function createRandomFilesAndDelete(dirName, number) {
  createDirectories(dirName).then((response) => {
    console.log(response);

    writeFiles(dirName, number).then((data) => {
      deleteFiles(dirName, data);
    });
  }).catch((err) => {
    console.log(err);
  })
}

module.exports = createRandomFilesAndDelete;
