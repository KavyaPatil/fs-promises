const fs = require("fs");
const path = require("path");
const dirname = "/home/kavya/Downloads/";
const filePath = path.join(dirname, "lipsum.txt");
const newFile1Path = path.join(__dirname, "newfile1.txt");
const filenamesPath = path.join(__dirname, "filenames.txt");
const newFile2Path = path.join(__dirname, "newfile2.txt");
const newFile3Path = path.join(__dirname, "newFile3.txt");
console.log(filePath);




function readFile(path) {

  return new Promise((resolve, reject) => {
    fs.readFile(path, "utf8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

function writeFile(data, path) {

  return new Promise((resolve, reject) => {
    fs.writeFile(path, data, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(path);
      }
    });
  });
}

function appendFile(data, path) {

  return new Promise((resolve, reject) => {

    fs.appendFile(path, data, "utf8", (err) => {
      if (err) {
        reject(err);
      } else {
        resolve("appended the file");
      }
    });

  })
  
}

function deleteFiles(data) {

  data.map((file) => {
    const filePath = path.join(__dirname, file);

    fs.unlink(filePath, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("deleted the file successfully");
      }
    });
  });
}


function workingWithLipsumTxtFile() {

  readFile(filePath).then((data) => {
    console.log(data);
    data = data.toUpperCase();
    console.log("read the file and converted to upperCase");

    writeFile(data, newFile1Path).then((path) => {
      let fNameArray = path.split("/");
      console.log(fNameArray);
      const fName = fNameArray[fNameArray.length - 1] + "\n";
      writeFile(fName, filenamesPath).then((res) => {
        console.log(res);

        readFile(newFile1Path).then((data) => {
          data = data.toLowerCase().split(".");
          data = JSON.stringify(data);
          console.log("read the file and converted to lower case");

          writeFile(data, newFile2Path).then((path) => {
            let fNameArray = path.split("/");
            console.log(fNameArray);
            const fName = fNameArray[fNameArray.length - 1] + "\n";

            appendFile(fName, filenamesPath).then((res) => {
              console.log(res);

              readFile(newFile2Path).then((data) => {
                data = JSON.parse(data).sort();
                data = JSON.stringify(data);
                console.log("sorted the data");

                writeFile(data, newFile3Path).then((path) => {
                  let fNameArray = path.split("/");
                  console.log(fNameArray);
                  const fName = fNameArray[fNameArray.length - 1] + "\n";

                  appendFile(fName, filenamesPath).then((res) => {
                    console.log(res);

                    readFile(filenamesPath).then((data) => {
                      data = data.split("\n");
                      data.pop();
                      deleteFiles(data);
                    }).catch((err) => {
                      console.log(err)
                    })
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

module.exports = workingWithLipsumTxtFile;
