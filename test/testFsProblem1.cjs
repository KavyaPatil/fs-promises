const path = require("path");

const fsProblem1 = require("../fs-problem1.cjs");

const directoryName = `../directory_${Math.floor(Math.random() * 10)}`;

const absolutePathOfRandomDirectory = path.resolve(directoryName);

const randomNumberOfFiles = 5;

const result = fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles);
